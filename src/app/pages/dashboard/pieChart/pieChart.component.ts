import {Component, ViewEncapsulation} from '@angular/core';

import {PieChartService} from './pieChart.service';

import  {SocketService} from  '../../socket.service';

import './pieChart.loader.ts';

@Component({
  selector: 'pie-chart',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./pieChart.scss')],
  template: require('./pieChart.html')
})
// TODO: move easypiechart to component
export class PieChart {
  connection;
  monitoring;

  public charts: Array<Object>;
  private _init = false;

  constructor(private _pieChartService: PieChartService,
  private  _socketService: SocketService) {
    this.charts = this._pieChartService.getData();
  }

  ngOnInit() {
    this.connection = this._socketService.getMonitoring().subscribe(message => {
      console.log("_______>");
      this.monitoring = JSON.parse(message);
      console.log(JSON.parse(message).systemUserName);

      this.charts[0].description = "CPU Used";
      this.charts[0].stats = ~~this.monitoring.coreCpuUsed;

      this.charts[1].description = "RAM Used";
      this.charts[1].stats = ~~((this.monitoring.coreUsedRam / this.monitoring.coreTotalRam) * 100);

      this.charts[2].description = "Battery";
      this.charts[2].stats = this.monitoring.coreBattery;
    });
  }

  ngAfterViewInit() {
    if (!this._init) {
      this._loadPieCharts();
      this._updatePieCharts();
      this._init = true;
    }
  }

  private _loadPieCharts() {

    jQuery('.chart').each(function () {
      let chart = jQuery(this);
      chart.easyPieChart({
        easing: 'easeOutBounce',
//        onStep: function (from, to, percent) {
  //        jQuery(this.el).find('.percent').text(Math.round(percent));
    //    },
        barColor: jQuery(this).attr('data-rel'),
        trackColor: 'rgba(0,0,0,0)',
        size: 84,
        scaleLength: 0,
        animation: 2000,
        lineWidth: 9,
        lineCap: 'round',
      });
    });
  }

  private _updatePieCharts() {
    let getRandomArbitrary = (min, max) => { return Math.random() * (max - min) + min; };

    jQuery('.pie-charts .chart').each(function(index, chart) {
      jQuery(chart).data('easyPieChart').update(getRandomArbitrary(55, 90));
    });
  }
}

import {Component, ViewEncapsulation, ElementRef} from '@angular/core';

import {Chart} from './trafficChart.loader.ts';
import {TrafficChartService} from './trafficChart.service';

import {SocketService} from  '../../socket.service';

@Component({
  selector: 'traffic-chart',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./trafficChart.scss')],
  template: require('./trafficChart.html')
})

// TODO: move chart.js to it's own component
export class TrafficChart {

  public doughnutData: Array<Object>;
  connection;
  public monitoring;
  public systemUserName;
  public systemName;
  public systemRelease;
  public systemVersion;
  constructor(private trafficChartService:TrafficChartService,
              private  socketService: SocketService) {
    this.doughnutData = trafficChartService.getData();
    this.connection = this.socketService.getMonitoring().subscribe(message => {
      console.log("_______>");
      this.monitoring = JSON.parse(message);
      console.log("_______> USER: ", this.monitoring.systemUserName);
      this.systemUserName = this.monitoring.systemUserName;
      this.systemName = this.monitoring.systemName;
      this.systemRelease = this.monitoring.systemRelease;
      this.systemVersion = this.monitoring.systemVersion;
  //    console.log("_______> USER" + this.monitoring.systemUserName);
//      console.log(JSON.parse(message).systemUserName);
    });
  }

  ngAfterViewInit() {
    this._loadDoughnutCharts();
  }

  private _loadDoughnutCharts() {
    let el = jQuery('.chart-area').get(0) as HTMLCanvasElement;
    new Chart(el.getContext('2d')).Doughnut(this.doughnutData, {
      segmentShowStroke: false,
      percentageInnerCutout : 64,
      responsive: true
    });
  }
}

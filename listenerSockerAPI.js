
var net = require('net');
var io = require('socket.io').listen(8991);

var server = net.createServer();
server.on('connection', handleConnection);

server.listen(8990, function() {
    console.log('server listening to %j', server.address());
});

function handleConnection(conn) {
    var remoteAddress = conn.remoteAddress + ':' + conn.remotePort;
    console.log('new client connection from %s', remoteAddress);

    conn.setEncoding('utf8');
    
    conn.on('data', onConnData);
    conn.once('close', onConnClose);
    conn.on('error', onConnError);

    function onConnData(d) {
	console.log(d);
	console.log('connection data from %s: %j', remoteAddress, d);
	io.sockets.on('connection', function (socket) {
	    socket.emit('monitoring', d);
	});
	conn.write(d);
    }

    function onConnClose() {
	console.log('connection from %s closed', remoteAddress);
    }

    function onConnError(err) {
	console.log('Connection %s error: %s', remoteAddress, err.message);
    }
}
